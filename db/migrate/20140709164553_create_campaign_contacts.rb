class CreateCampaignContacts < ActiveRecord::Migration
  def change
    create_table :campaign_contacts do |t|
      t.belongs_to :campaign
      t.belongs_to :creator
      t.belongs_to :contact
      t.timestamps
    end
  end
end
