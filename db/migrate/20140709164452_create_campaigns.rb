class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :story
      t.string :date_of
      t.string :image
      t.belongs_to :creator
      t.belongs_to :campaign_contacts
      t.timestamps
    end
  end
end
