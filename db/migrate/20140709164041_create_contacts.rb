class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :firstName
      t.string :lastName
      t.string :email
      t.string :phone
      t.string :address
      t.string :suite
      t.string :city
      t.string :state
      t.string :zip
      t.belongs_to :campaign_contacts
      t.timestamps
    end
  end
end
