class StaticController < ApplicationController
	def index
		@current = current_creator
	end

	def link
		@current = current_creator
		@link = Campaign.last
	end

	def invite
		@current = current_creator
		@campaign = Campaign.find(params[:id])
	end
	
	def thankyou
		@current = current_creator
	end

	def registry
		@current = current_creator
	end
end