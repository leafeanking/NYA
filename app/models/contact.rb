class Contact < ActiveRecord::Base
	has_many :campaign_contacts
	has_many :campaigns, through: :campaign_contacts

	validates :firstName, presence: true
	validates :lastName, presence: true
	validates :address, presence: true
	validates :city, presence: true
	validates :state, presence: true
	validates :zip, presence: true

end
