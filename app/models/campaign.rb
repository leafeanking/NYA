class Campaign < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	has_many :campaign_contacts
	has_many :contacts, through: :campaign_contacts

	belongs_to :creator
	validates :story, presence: true
	validates :date_of, presence: true
	validates :image, presence: true
end
