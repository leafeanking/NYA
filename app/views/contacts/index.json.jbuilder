json.array!(@contacts) do |contact|
  json.extract! contact, :id, :firstName, :lastName, :email, :phone, :address, :suite, :city, :state, :zip
  json.url contact_url(contact, format: :json)
end
