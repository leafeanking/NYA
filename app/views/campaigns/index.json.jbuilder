json.array!(@campaigns) do |campaign|
  json.extract! campaign, :id, :story, :date_of, :image
  json.url campaign_url(campaign, format: :json)
end
