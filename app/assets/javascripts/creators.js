app = angular.module('CreatorsApp', ['ngResource']);

app.config([
  '$httpProvider', function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
  }
]);

app.factory('Creator', ['$resource', function($resource) {
  var creator = $resource('/creators/:id', { id: '@id' }, {
    update: { method: 'PATCH' }
  });
  return creator;
}]);

app.factory('Creators', ['$resource', function($resource) {
  var creators = $resource('/creators', {}, {
      query: {method:'GET', isArray:true}
  });
  return creators;
}]);

app.factory('Contacts', ['$resource', function($resource) {
  var contacts = $resource('/contacts', {}, {
      query: {method:'GET', isArray:true}
  });
  return contacts;
}]);

app.controller('CreatorCtrl', ['$scope', 'Creators',  function($scope, Creators){
	console.log("CreatorCtrl is running");
	$scope.creators = Creators.query();

}])

app.controller('ContactCtrl',  ['$scope', 'Contacts', function($scope, Contacts){
  console.log('ContactCtrl is running.');
  $scope.contacts = Contacts.query();
  console.log($scope.contacts)

}])