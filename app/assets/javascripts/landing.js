$(document).ready( function() {

    $(window).scroll(function() {
        if ($('.navbar') && $(".navbar").offset().top > 50) {
            $(".navbar-fixed-top").addClass("top-nav-collapse");
        } else {
            $(".navbar-fixed-top").removeClass("top-nav-collapse");
        }
    });


    $(function() {
        $('.page-scroll a').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });


});

$(document).ready(function() {
    var offset = 220;
    var duration = 500;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });
    
    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    })

    $('.modal-body div').each( function(){
      console.log($(this))
    })

    
    $('#sli1').click(function(){
        $('#sli1').fadeOut(1000);
      })
    $('#sli2').click(function(){
        $(this).fadeOut(1000);
      })
    $('#sli3').click(function(){
        $(this).fadeOut(1000);
      })
    $('#sli4').click(function(){
        $(this).fadeOut(1000);
      })
    $('#sli5').click(function(){
        $(this).fadeOut(1000);
      })
    $('#sli6').click(function(){
        $(this).fadeOut(1000);
      })
    $('#sli7').click(function(){
        $(this).fadeOut(1000);
      })
    $('#sli8').click(function(){
        $(this).fadeOut(1000);
      })
    $('#sli9').click(function(){
        $('#sli9').fadeOut(1000);
      })
  });

angular.module('GetStarted', ['ui.bootstrap']);
var GetStartedCtrl = function ($scope, $modal, $log) {

  $scope.items = ['Step 1', 'Step 2', 'Step 3', 'Step 4'];

  $scope.open = function (size) {

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      size: size,
      resolve: {
        items: function () {
          return false;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };
};

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

var ModalInstanceCtrl = function ($scope, $modalInstance, items) {

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};

