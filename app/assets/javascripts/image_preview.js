$(function() {
  $("#campaign_image").change(function() {
    readURL(this);
    console.log('file field' , $('campaign_image'))
  });


  $(".remove_image").click(function() {
    removeURL(this, $("#prev_image"));
  });
});

//To show image preview
readURL = function(input) {
  //console.log(input)
  var reader;
  if (input.files && input.files[0]) {
    reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.onload = function(e) {
      $("#img_prev").attr("src", e.target.result)
      .css({
        display: "block"
      });

    };
  }
};

//To remove image preview and image from file field.

removeURL = function( input) {
  $(input).val('');
  $("#img_prev").css({
    display: "none"
  });
  /*return $(remove_button).css({
    display: "none"
  });*/
};